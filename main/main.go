package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

var BASE_URL = "http://13.59.63.30:3000"

type Task struct {
	Number int `json:"number"`
}

type Result struct {
	Number   int    `json:"number"`
	Prime    int    `json:"prime"`
	Result   int    `json:"result"`
	Language string `json:"language"`
	Log      string `json:"log"`
}

func main() {

	lastTask := &Task{
		Number: -1,
	}

	for range time.Tick(time.Second) {
		task, err := getCurrentTask()
		if err != nil {
			fmt.Printf("Error getting task: %s\n", err.Error())
			continue
		}

		err = handleTask(task, lastTask)
		if err != nil {
			fmt.Printf("Error handling task: %s\n", err.Error())
			continue
		}

		lastTask = task
	}
}

func getCurrentTask() (*Task, error) {
	resp, err := http.Get(BASE_URL + "/task")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("error getting task: %s", body)
	}

	var task Task
	err = json.Unmarshal(body, &task)
	if err != nil {
		return nil, err
	}

	return &task, nil
}

func handleTask(task *Task, lastTask *Task) error {
	if task.Number == lastTask.Number {
		return nil
	}

	taskOutput, err := runTask(task)
	if err != nil {
		return err
	}

	err = handleResult(task, taskOutput)
	return err
}

func runTask(task *Task) ([]byte, error) {
	command := exec.Command("./run.sh", strconv.Itoa(task.Number))
	output, err := command.Output()
	return output, err
}

func handleResult(task *Task, resultBytes []byte) error {
	result, err := parseResult(task, string(resultBytes))
	if err != nil {
		return err
	}
	err = submitResult(result)
	fmt.Printf("Result submitted: %+v\n", result)
	return err
}

func parseResult(task *Task, result string) (*Result, error) {
	parts := strings.SplitN(result, ",", 4)
	if len(parts) != 4 {
		return nil, fmt.Errorf("incomplete result string: %s", result)
	}

	prime, err := strconv.Atoi(parts[0])
	if err != nil {
		return nil, fmt.Errorf("could not parse prime from result string %s", result)
	}

	count, err := strconv.Atoi(parts[1])
	if err != nil {
		return nil, fmt.Errorf("could not parse factor count from result string %s", result)
	}

	return &Result{
		Number:   task.Number,
		Prime:    prime,
		Result:   count,
		Language: parts[2],
		Log:      parts[3],
	}, nil
}

func submitResult(result *Result) error {
	reqBody, err := json.Marshal(result)
	if err != nil {
		return err
	}
	reqBodyReader := bytes.NewReader(reqBody)

	resp, err := http.Post(BASE_URL+"/result", "application/json", reqBodyReader)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("error posting result: %s", body)
	}

	return nil
}
